package models.playables;

import models.IPlayable;
import models.Move;
import models.UserInput;

public class ConsolePlayable implements IPlayable {

    private UserInput userInput;

    public ConsolePlayable(UserInput userInput) {
        this.userInput = userInput;
    }

    @Override
    public Move getCurrentMove() {
        return Move.valueOf(userInput.getStringInput());
    }
}
