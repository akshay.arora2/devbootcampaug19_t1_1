package models;

import java.util.HashMap;
import java.util.Map;

public class Game {
    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfGames;
    private final GameOutput gameOutput;
    private final Map<Integer, Scores> gameScores = new HashMap<>();

    public Game(Player player1, Player player2, Machine machine, int noOfGames, GameOutput gameOutput) {

        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfGames = noOfGames;
        this.gameOutput = gameOutput;
    }

    public void play() {
        for(int i = 0; i < noOfGames; i++) {
            calculateAndStoreRoundScore(i);
        }
    }

    public void printScore() {
        gameOutput.printString(String.format("Player1 : %d, Player2 : %d", player1.getScore(), player2.getScore()));
    }


    public void printScoreTable() {

        Map<Integer, Scores> cumulativeRoundScore = calculateCumulativeRoundScore();

        StringBuilder stringBuilder = new StringBuilder();
        for(Map.Entry<Integer,Scores> entry : cumulativeRoundScore.entrySet()) {
            if(stringBuilder.length() > 0) {
                stringBuilder.append("\n");
            }
            stringBuilder.append(String.format("%d\t%d\t%d", entry.getKey() + 1, entry.getValue().getPlayer1Score(), entry.getValue().getPlayer2Score()));
        }
        gameOutput.printString(stringBuilder.toString());
    }

    private Map<Integer, Scores> calculateCumulativeRoundScore() {
        Map<Integer, Scores> result = new HashMap<Integer, Scores>();
        Scores cumulativeStepScore = null;
        for(Map.Entry<Integer,Scores> entry : gameScores.entrySet()) {
            if(cumulativeStepScore == null) {
                cumulativeStepScore = new Scores(entry.getValue().getPlayer1Score(), entry.getValue().getPlayer2Score());
            } else {
                cumulativeStepScore = new Scores(cumulativeStepScore.getPlayer1Score() + entry.getValue().getPlayer1Score(), cumulativeStepScore.getPlayer2Score() + entry.getValue().getPlayer2Score());
            }
            result.put(entry.getKey(), cumulativeStepScore);
        }
        return result;
    }

    private void calculateAndStoreRoundScore(int round) {
        Scores scores = machine.calculateScore(player1.getCurrentMove(), player2.getCurrentMove());
        player1.addScore(scores.getPlayer1Score());
        player2.addScore(scores.getPlayer2Score());
        gameScores.put(round, scores);
    }
}
