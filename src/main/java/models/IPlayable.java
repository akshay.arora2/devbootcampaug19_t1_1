package models;

public interface IPlayable {

    Move getCurrentMove();
}
