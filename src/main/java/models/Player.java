package models;

public class Player {

    private int score;

    private IPlayable iPlayable;

    public Player(IPlayable iPlayable) {
        this.iPlayable = iPlayable;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }

    public Move getCurrentMove() {
        return iPlayable.getCurrentMove();
    }
}
