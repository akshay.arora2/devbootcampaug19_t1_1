import models.*;
import models.playables.ConsolePlayable;

public class GameController {

    public static void main(String[] args) {

        //runGameWithConsolePlayers();
        runGameWithBehaviourPlayers();
    }

    private static void runGameWithConsolePlayers() {
        ScannerUserInput scannerUserInput = new ScannerUserInput();

        Game game = new Game(new Player(new ConsolePlayable(scannerUserInput)), new Player(new ConsolePlayable(scannerUserInput)), new Machine(), 2, new GameOutput() {
            @Override
            public void printString(String output) {
                System.out.println(output);
            }
        });

        game.play();
        game.printScoreTable();
    }

    private static void runGameWithBehaviourPlayers() {

        Game game = new Game(new Player(() -> Move.COOPERATE), new Player(() -> Move.CHEAT), new Machine(), 5, new GameOutput() {
            @Override
            public void printString(String x) {
                System.out.println(x);
            }
        });

        game.play();
        game.printScoreTable();
    }
}
