package models;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;

public class GameIntegrationTest {

    @Test
    public void testGame() {
        PrintStream printStream = Mockito.mock(PrintStream.class);
        Game game = new Game(new Player(() -> Move.COOPERATE), new Player(() -> Move.COOPERATE), new Machine(), 5, output -> printStream.print(output));
        game.play();
        game.printScore();

        Mockito.verify(printStream).print("Player1 : 10, Player2 : 10");
    }
}
